\version "2.19.49"
% automatically converted by musicxml2ly

% clairnote-type = sn
\include "clairnote.ly"

\header {
  encodingsoftware = "MuseScore 2.0.2"
  encodingdate = "2015-10-07"
  composer = "Anonymous"
  title = "The Cobbler"
  subtitle = "for guitar"
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
}

#(set-global-staff-size 20)

\paper {
  paper-width = 21.01 \cm
  paper-height = 29.69 \cm
  top-margin = 1.0 \cm
  bottom-margin = 2.0 \cm
  left-margin = 1.0 \cm
  right-margin = 1.0 \cm
  system-system-spacing.basic-distance = #24
}

PartPOneVoiceOne =  \relative e'' {
  \clef "treble" \key a \major \numericTimeSignature\time 4/4 \partial
  4 e4 | % 1
  a4 a4 gis8. a16 b4 | % 2
  fis2 e2 | % 3
  fis4 fis4 gis4 fis8 gis8 | % 4
  a2 e4 e4 | % 5
  a4 gis8 fis8 gis8 a8 b4 | % 6
  fis8 dis8 dis8 fis8 e8 b8 gis8 e'8 \break | % 7
  fis8 dis8 e8 fis8 gis8 e8 fis8 gis8 | % 8
  a4 gis8 fis8 e4 fis8 gis8 | % 9
  a4 e4 fis4 e8 d8 | \barNumberCheck #10
  cis4. d8 e4 d8 cis8 | % 11
  b4. cis8 d4 e8 d8 | % 12
  cis4. b8 a4 b4 \break | % 13
  cis4. b8 a4 b4 | % 14
  cis8 d8 e4 <a, cis>4 <gis b>4 | % 15
  a2. e'4 | % 16
  a8 e8 e8 g8 fis4 e8 d8 | % 17
  cis8 b8 cis8 d8 e4 d8 cis8 | % 18
  b8 a8 b8 cis8 d8 b8 e8 d8 \break | % 19
  cis8 d8 cis8 b8 a8 gis8 a8 b8 | \barNumberCheck #20
  cis8 b8 a8 gis8 fis8 gis8 a8 b8 | % 21
  cis8 d8 e8 d8 <a cis>4 <gis b>4 | % 22
  <e a>2. e'4 | % 23
  a4. a8 gis4 b4 \break | % 24
  fis4 fis4 <b, e>4. e8 | % 25
  fis4. fis8 gis4. gis8 | % 26
  a4. fis8 e4. e8 | % 27
  a4. a8 gis4. gis8 \break | % 28
  fis4. fis8 e4. e8 | % 29
  fis4. fis8 gis4. gis8 | \barNumberCheck #30
  a4. a8 e4. e8 | % 31
  a4 e8 gis8 fis4 e8 d8 \break | % 32
  cis4 a8 b8 cis8 d8 e8 cis8 | % 33
  b4. cis8 d8 b8 e8 d8 | % 34
  cis4 b8 cis8 a4 b4 | % 35
  cis8 b8 a8 gis8 fis8 gis8 a8 b8 | % 36
  cis8 d8 e8 d8 cis8 a8 b8 gis8 | % 37
  a2. e'4 \break | % 38
  fis8 gis8 a8 e8 g4. fis8 | % 39
  e8 d8 cis8 b8 cis8 d8 e8 fis8 | \barNumberCheck #40
  gis8 e8 fis8 d8 e8 cis8 d8 b8 | % 41
  cis4. b8 a4. b8 \break | % 42
  cis4. b8 a4. b8 | % 43
  cis4. d8 e4. d8 | % 44
  <a cis>1 \bar "|."
}

PartPOneVoiceThree =  \relative a {
  \clef "treble" \key a \major \numericTimeSignature\time 4/4 \partial
  4 s4*25 \break s1*6 \break s1*6 \break s1*4 | % 23
  a2 e2 \break | % 24
  b'2 e,2 | % 25
  b'2 e,2 | % 26
  a1 s1 \break s1*4 \break s1*6 \break s1*4 \break s1*3 \bar "|."
}

PartPOneVoiceTwo =  \relative a {
  \clef "treble" \key a \major \numericTimeSignature\time 4/4 \partial
  4 s4 | % 1
  \override Stem.direction = #DOWN
  <a cis'>2 <e b''>2 | % 2
  <b' b'>2 <e, gis' b>2 | % 3
  <b' b'>2 <e, b''>2 | % 4
  <a cis'>2. a4\rest | % 5
  a2 e2 | % 6
  b'2 e,2 \break | % 7
  b'2 e,2 | % 8
  a1 | % 9
  a2 d2 | \barNumberCheck #10
  a1 | % 11
  e1 | % 12
  a2. e4 \break | % 13
  a2. e4 | % 14
  a2 a4 gis4 | % 15
  <a e'>1 | % 16
  a2 ~ a8 d8 ~ d4 | % 17
  a1 | % 18
  e1 \break | % 19
  a1 | \barNumberCheck #20
  a2. e4 | % 21
  a2 a4 e4 | % 22
  a2. a'4\rest | % 23
  c8\rest e8 cis4 c8\rest e4 b8 \break | % 24
  b8\rest dis4 b8 gis4 e4 | % 25
  b'8\rest dis8 b4 c8\rest b8 e,4 | % 26
  a8\rest cis8 a4 a8\rest cis8 a4 | % 27
  c8\rest cis8 a,4 cis8\rest b'8 e,,4 \break | % 28
  e'8\rest b'8 b,4 e8\rest b'8 e,,4 | % 29
  e'8\rest b'8 b,4 e'8\rest b8 e,,4 | \barNumberCheck #30
  d'8\rest cis'8 a,4 e8\rest cis''8 a,4 | % 31
  a2 d2 \break | % 32
  a1 | % 33
  e1 | % 34
  a2. e4 | % 35
  a2. e4 | % 36
  a2 a4 e4 | % 37
  <a e'>1 \break | % 38
  a2 g'8\rest b8 d,4 | % 39
  a1 | \barNumberCheck #40
  e1 | % 41
  f'8\rest  a8 a,4 r8 e'8 e,4 \break | % 42
  f'8\rest  a8 a,4 d8\rest e'8 e,4 | % 43
  f8\rest a8 a,4 f'8\rest gis8 e,4 | % 44
  <a e'>1 \bar "|."
}


% The score definition
\score {
  <<
    \new Staff <<

      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceThree" { \voiceTwo \PartPOneVoiceThree }
        \context Voice = "PartPOneVoiceTwo" { \voiceThree \PartPOneVoiceTwo }
      >>
    >>

  >>
  \layout {
    \context {
      \Staff
      \cnStaffOctaveSpan 3
    }
  }

  \midi {}
}

