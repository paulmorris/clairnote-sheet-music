\version "2.19.49"
% automatically converted by musicxml2ly from C:/Users/Willem/Documents/muziek/musescore bestanden/Morrisons_Jig.xml

%% additional definitions required by the score:
m = #(make-dynamic-script "m")
% clairnote-type = sn
\include "clairnote.ly"
\header {
  copyright = "2015"
  encodingdate = "2015-09-13"
  title = "Morrison's Jig"
  source = "http://musescore.com/score/1160201"
  composer = "Traditional Irish Jig Arr. for harp by, Jacquelynn Tietjen"
  encodingsoftware = "MuseScore 2.0.2"
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
}

PartPOneVoiceOne =  \relative e' {
  \repeat volta 2 {
    \clef "treble" \key d \major \time 6/8 | % 1
    \tempo 4=168 e4. \m b'4. | % 2
    e,4 b'8 a8 fis8 d8 | % 3
    e8 b'8 e,8 b'4 b8 | % 4
    d8 cis8 b8 a8 fis8 d8 | % 5
    e4. b'4. | % 6
    e,4 b'8 a8 fis8 d8 | % 7
    g8 a8 g8 fis8 g8 a8 | % 8
    b8 a8 g8 fis8 e8 d8
  }
  \repeat volta 2 {
    | % 9
    b'8 \mf e8 e8 fis8 e8 e8 | \barNumberCheck #10
    a8 e8 e8 fis8 e8 d8 | % 11
    b8 e8 e8 fis8 e8 e8 | % 12
    a4 g8 fis8 e8 d8 | % 13
    b8 e8 e8 fis8 e8 e8 | % 14
    a8 e8 e8 fis8 e8 fis8 | % 15
    g8 fis8 e8 d4 a8 | % 16
    b8 a8 g8 fis8 e8 d8 | % 17
    b'8 e8 e8 fis8 e8 e8 | % 18
    a8 e8 e8 fis8 e8 d8 | % 19
    b8 e8 e8 fis8 e8 e8 | \barNumberCheck #20
    fis8 a8 fis8 d8 e8 fis8 | % 21
    g4. g8 fis8 g8 | % 22
    d8 e8 fis8 g8 fis8 g8 | % 23
    e8 d8 cis8 d4 a8 | % 24
    b8 a8 g8 fis8 e8 d8
  }
  \repeat volta 2 {
    | % 25
    \ottava #1 | % 25
    e'4. \mf b'4. | % 26
    e,4 b'8 a8 fis8 d8 \ottava #0 | % 27
    \ottava #1 | % 27
    e8 b'8 e,8 b'4 b8 | % 28
    d8 cis8 b8 a8 fis8 d8 | % 29
    e4. b'4. | \barNumberCheck #30
    e,4 b'8 a8 fis8 d8 | % 31
    g8 a8 g8 fis8 g8 a8 | % 32
    b8 a8 g8 fis8 e8 d8 \ottava #0
  }
  \repeat volta 2 {
    | % 33
    b8 \mf e8 e8 fis8 e8 e8 | % 34
    a8 e8 e8 fis8 e8 d8 | % 35
    b8 e8 e8 fis8 e8 e8 | % 36
    a4 g8 fis8 e8 d8 | % 37
    b8 e8 e8 fis8 e8 e8 | % 38
    a8 e8 e8 fis8 e8 fis8 | % 39
    g8 fis8 e8 d4 a8 | \barNumberCheck #40
    b8 a8 g8 fis8 e8 d8 | % 41
    b'8 e8 e8 fis8 e8 e8 | % 42
    a8 e8 e8 fis8 e8 d8 | % 43
    b8 e8 e8 fis8 e8 e8 | % 44
    fis8 a8 fis8 d8 e8 fis8 | % 45
    g4. g8 fis8 g8 | % 46
    d8 e8 fis8 g8 fis8 g8 | % 47
    e8 d8 cis8 d4 a8 | % 48
    b8 a8 g8 fis8 e8 d8
  }
  | % 49
  e2. \m | \barNumberCheck #50
  e4. fis4. | % 51
  e2. | % 52
  d'4. a4. | % 53
  e4. b'4. | % 54
  e,4 b'8 a8 fis8 d8 | % 55
  g8 a8 g8 fis8 g8 a8 | % 56
  b8 a8 g8 fis8 e8 d8 | % 57
  e4. b'4. | % 58
  e,4 b'8 a8 fis8 d8 | % 59
  e8 b'8 e,8 b'4 b8 | \barNumberCheck #60
  d8 cis8 b8 a8 fis8 d8 | % 61
  e4. b'4. | % 62
  e,4. fis4. | % 63
  g2 a4 | % 64
  fis4. d4. \repeat volta 2 {
    | % 65
    b'8 \mf e8 e8 fis8 e8 e8 | % 66
    a8 e8 e8 fis8 e8 d8 | % 67
    b8 e8 e8 fis8 e8 e8 | % 68
    a4 g8 fis8 e8 d8 | % 69
    b8 e8 e8 fis8 e8 e8 | \barNumberCheck #70
    a8 e8 e8 fis8 e8 fis8 | % 71
    g8 fis8 e8 d4 a8 | % 72
    b8 a8 g8 fis8 e8 d8 | % 73
    b'8 e8 e8 fis8 e8 e8 | % 74
    a8 e8 e8 fis8 e8 d8 | % 75
    b8 e8 e8 fis8 e8 e8 | % 76
    fis8 a8 fis8 d8 e8 fis8 | % 77
    g4. g8 fis8 g8 | % 78
    d8 e8 fis8 g8 fis8 g8 | % 79
    e8 d8 cis8 d4 a8 | \barNumberCheck #80
    b8 a8 g8 fis8 e8 d8
  }
  | % 81
  \ottava #1 | % 81
  e'4. \m b'4. \ottava #0 | % 82
  \ottava #1 | % 82
  e,4 b'8 a8 fis8 d8 | % 83
  e8 b'8 e,8 b'4 b8 | % 84
  d8 cis8 b8 a8 fis8 d8 | % 85
  e4. b'4. | % 86
  e,4 b'8 a8 fis8 d8 | % 87
  g8 a8 g8 fis8 g8 a8 | % 88
  b8 a8 g8 fis8 e8 d8 \ottava #0 | % 89
  \ottava #1 | % 89
  e4. b'4. | \barNumberCheck #90
  e,4 b'8 a8 fis8 d8 | % 91
  e8 b'8 e,8 b'4 b8 | % 92
  d8 cis8 b8 a8 fis8 d8 | % 93
  e4. \mp b'4. | % 94
  e,4 b'8 a8 fis8 d8 | % 95
  g8 a8 g8 fis8 g8 a8 \ottava #0 | % 96
  \ottava #1 | % 96
  b8 -\markup{ \bold\italic {rit.} } a8 g8 fis8 e8 d8 | % 97
  e2. ~ | % 98
  e2. \ottava #0 \bar "|."
}

PartPOneVoiceFive =  \relative e {
  \repeat volta 2 {
    \clef "bass" \key d \major \time 6/8 e2. ~ | % 2
    e2. | % 3
    e2. | % 4
    d'2. | % 5
    e,2. ~ | % 6
    e2. | % 7
    g2 a4 | % 8
    b4. d4.
  }
  \repeat volta 2 {
    | % 9
    <e,, e'>2. ~ ~ | \barNumberCheck #10
    <e e'>2. | % 11
    <e e'>2. | % 12
    <d' d'>2. | % 13
    <e, e'>2. ~ ~ | % 14
    <e e'>2. | % 15
    g'2 a4 | % 16
    b4. d4. | % 17
    <e,, e'>2. ~ ~ | % 18
    <e e'>2. | % 19
    <e e'>2. | \barNumberCheck #20
    b''4. d4. | % 21
    g,2. | % 22
    b4. d4. | % 23
    a4. d4. | % 24
    b4. d4.
  }
  \repeat volta 2 {
    | % 25
    <e,, e'>2. ~ ~ | % 26
    <e e'>2. | % 27
    <e e'>2. | % 28
    <d' d'>2. | % 29
    <e, e'>2. ~ ~ | \barNumberCheck #30
    <e e'>2. | % 31
    <g g'>2 <a a'>4 | % 32
    <b b'>4. <d d'>4.
  }
  \repeat volta 2 {
    | % 33
    <e, e'>2. ~ ~ | % 34
    <e e'>2. | % 35
    <e e'>2. | % 36
    <d' d'>2. | % 37
    <e, e'>2. ~ ~ | % 38
    <e e'>2. | % 39
    <g g'>2 <a a'>4 | \barNumberCheck #40
    <b b'>4. <d d'>4. | % 41
    <e, e'>2. ~ ~ | % 42
    <e e'>2. | % 43
    <e e'>2. | % 44
    <b' b'>4. <d d'>4. | % 45
    <g, g'>2. | % 46
    <b b'>4. <d d'>4. | % 47
    <a a'>4. <d d'>4. | % 48
    <b b'>4. <d d'>4.
  }
  | % 49
  e4. b'4. | \barNumberCheck #50
  e,4 b'8 a8 fis8 d8 | % 51
  e8 b'8 e,8 b'4 b8 | % 52
  d8 cis8 b8 a8 fis8 e8 | % 53
  e4. b'4. | % 54
  e,4 b'8 a8 fis8 e8 | % 55
  g2 a4 | % 56
  fis4. d4. | % 57
  e2. | % 58
  e4. fis4. | % 59
  e4. b'4. | \barNumberCheck #60
  d4. cis4. | % 61
  e,4. b'4. | % 62
  e,4 b'8 a8 fis8 e8 | % 63
  g8 a8 g8 fis8 g8 a8 | % 64
  b8 a8 g8 fis8 e8 d8 \repeat volta 2 {
    | % 65
    <e, e'>2. ~ ~ | % 66
    <e e'>2. | % 67
    <e e'>2. | % 68
    <d' d'>2. | % 69
    <e, e'>2. ~ ~ | \barNumberCheck #70
    <e e'>2. | % 71
    <g g'>2 <a a'>4 | % 72
    <b b'>4. <d d'>4. | % 73
    <e, e'>2. ~ ~ | % 74
    <e e'>2. | % 75
    <e e'>2. | % 76
    <b' b'>4. <d d'>4. | % 77
    <g, g'>2. | % 78
    <b b'>4. <d d'>4. | % 79
    <a a'>4. <d d'>4. | \barNumberCheck #80
    <b b'>4. <d d'>4.
  }
  | % 81
  e2. | % 82
  e4. fis4. | % 83
  e4. b'4. | % 84
  d4. cis4. | % 85
  e,4. b'4. | % 86
  e,4 b'8 a8 fis8 e8 | % 87
  g2 a4 | % 88
  fis4. d4. | % 89
  e2. | \barNumberCheck #90
  e4. fis4. | % 91
  e4. b'4. | % 92
  d4. cis4. | % 93
  e,4. b'4. | % 94
  e,4 b'8 a8 fis8 e8 | % 95
  g2 a4 | % 96
  fis4. d4. | % 97
  e2. | % 98
  e,2. \p \bar "|."
}


% The score definition
\score {
  <<
    \new PianoStaff <<
      \set PianoStaff.instrumentName = "Harp"
      \set PianoStaff.shortInstrumentName = "Hrp."
      \context Staff = "1" <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
      >> \context Staff = "2" <<
        \context Voice = "PartPOneVoiceFive" { \PartPOneVoiceFive }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

