\version "2.19.49"
% automatically converted by musicxml2ly from C:/Users/Willem/Documents/muziek/musescore bestanden/The_Irish_Washerwoman.xml
% clairnote-type = sn
\include "clairnote.ly"
\header {
  title = "The Irish Washerwoman"
  meter = "G major"
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
}

PartPOneVoiceOne =  \relative d'' {
  \clef "treble" \key g \major \time 6/8
  \partial 8 d16 c16
  \repeat volta 2
  {
    | % 2
    b8 g8 g8 d8 g8 g8 | % 3
    b8 g8 b8 d8 c8 b8 | % 4
    c8 a8 a8 d,8 a'8 a8 | % 5
    c8 a8 c8 e8 d8 c8 | % 6
    b8 g8 g8 d8 g8 g8 | % 7
    b8 g8 b8 d8 c8 b8 | % 8
    c8 b8 c8 a8 d8 c8
  }
  \alternative {
    {
      | % 9
      b8 g8 g8 g4 d'16 c16
    }
    {
      |
      b8 g8 g8 g4 g'16 a16
    }
  } \repeat volta 2 {
    | % 11
    b8 g8 g8 d8 g8 g8 | % 12
    b8 g8 b8 b8 a8 g8 | % 13
    a8 fis8 fis8 d8 fis8 fis8 | % 14
    a8 fis8 a8 a8 g8 fis8 | % 15
    e8 g8 g8 d8 g8 g8 | % 16
    c,8 g'8 g8 b,8 g'8 g8 | % 17
    c,8 b8 c8 a8 d8 c8
  }
  \alternative {
    {
      | % 18
      b8 g8 g8 g4 g'16 a16
    }
    {
      | % 19
      b,8 g8 g8 g4.
    }
  }
}


% The score definition
\score {
  <<
    \new Staff <<

      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

