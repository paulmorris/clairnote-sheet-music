\version "2.19.49"
% automatically converted by musicxml2ly

% clairnote-type = sn
\include "clairnote.ly"

#(set-global-staff-size 20)

\paper {
  paper-width = 21.01 \cm
  paper-height = 29.69 \cm
  top-margin = 1.0 \cm
  bottom-margin = 2.0 \cm
  left-margin = 1.0 \cm
  right-margin = 1.0 \cm
  system-system-spacing.basic-distance = #24
}

\header {
  title = "We Wish You a Merry Christmas"
  subtitle = " "
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
  encodingsoftware = "MuseScore 2.0.2"
  encodingdate = "2015-11-27"
}

PartPOneVoiceOne =  \relative c' {
  \clef "treble" \key f \major \time 3/4 \partial 4 c4 | % 1
  f4 f8 g8 f8 e8 | % 2
  d4 bes4 d4 | % 3
  g4 g8 a8 g8 f8 | % 4
  e4 c4 e4 | % 5
  a4 a8 bes8 a8 g8 | % 6
  f4 d4 c8 c8 | % 7
  <d c'>4 <g c>4 <e bes'>4 | % 8
  <f a>2 c4 | % 9
  <f c>4 <f c>4 <f c>4 | \barNumberCheck #10
  <e c>2 <e c>4 | % 11
  <f f>4 <e g>4 <d g>4 | % 12
  <c g'>2 g'4 | % 13
  a4 g4 f4 | % 14
  <c' e>4 <c, a'>4 <c a'>8 <c a'>8 | % 15
  <d c' c,>4 <g c c,>4 <e bes' bes,>4 | % 16
  <f a a,>2. \bar "|."
}

PartPOneVoiceOneLyricsOne =  \lyricmode {
  we wish you a mer -- ry christ
  -- mas we wish you a mer -- ry christ mas we wish you a mer -- ry
  christ -- mas and a hap -- py new year good ti -- dings we bring to
  you and your kin good ti -- dings for christ -- mas and a hap -- py
  new year
}

PartPOneVoiceOneLyricsTwo =  \lyricmode {
  oh bring us some fig -- gy pud
  -- ding oh bring us some fig -- gy pud -- ding oh bring us some fig
  -- gy pud -- ding and \skip4 bring it out here
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
}

PartPTwoVoiceOne =  \relative c {
  \clef "treble_8" \key f \major \time 3/4 \partial 4
  % \override Stem.direction = #DOWN
  c4 | % 1
  f4 f8 g8 f8 e8 | % 2
  d4 bes4 d4 | % 3
  g4 g8 a8 g8 f8 | % 4
  e4 c4 e4 | % 5
  a4 a8 bes8 a8 g8 | % 6
  f4 d4 c8 c8 | % 7
  bes4 bes4 c4 | % 8
  f4 r4 f4 | % 9
  a4 a4 a4 | \barNumberCheck #10
  g2 g4 | % 11
  g4 g4 b4 | % 12
  c2 c4 | % 13
  c4 bes4 a4 | % 14
  a4 c4 c8 c8 | % 15
  bes4 bes4 bes4 | % 16
  a2. \bar "|."
}


% The score definition
\score {
  <<
    \new Staff <<
      \set Staff.instrumentName = "Alto"
      \set Staff.shortInstrumentName = "A."
      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsOne
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsTwo
      >>
    >>
    \new Staff <<
      \set Staff.instrumentName = "Tenor"
      \set Staff.shortInstrumentName = "T."
      \context Staff <<
        \context Voice = "PartPTwoVoiceOne" { \PartPTwoVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

