\version "2.19.49"
% automatically converted by musicxml2ly from C:/Users/Willem/Documents/muziek/musescore bestanden/the Hag's Purse.xml
% clairnote-type = sn
\include "clairnote.ly"
\header {
  encodingsoftware = "MuseScore 2.0.2"
  source = "https://musescore.com/score/1209736"
  encodingdate = "2015-09-16"
  title = "The Hag's Purse"
  subtitle = " "
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
}

\layout {
  \context {
    \Score
    skipBars = ##t
  }
}
PartPOneVoiceOne =  \relative a' {
  \clef "treble" \key d \major \time 6/8 | % 1
  a4 d,8 fis8 e8 d8 | % 2
  a'4 b8 c8 a8 fis8 | % 3
  g4 e8 e8 d8 e8 | % 4
  c'4 d8 c8 a8 g8 | % 5
  a4 d,8 fis8 e8 d8 | % 6
  a'4 b8 c8 a8 fis8 | % 7
  g8 e8 e8 c'8 e,8 e8 | % 8
  d8 e8 d8 d4. | % 9
  d'4 e8 f8 e8 d8 | \barNumberCheck #10
  d8 e8 d8 c8 a8 fis8 | % 11
  g4 e8 e8 d8 e8 | % 12
  c'4 d8 c8 a8 g8 | % 13
  a8 d8 e8 f8 e8 d8 | % 14
  d8 e8 d8 c8 a8 fis8 | % 15
  g8 e8 e8 c'8 e,8 e8 | % 16
  d8 e8 d8 d4. | % 17
  \bar "|."
}

PartPOneVoiceOneChords =  \chordmode {
  | % 1
  d4:5 s8 s8 s8 s8 | % 2
  s4 s8 s8 s8 s8 | % 3
  c4:5 s8 s8 s8 s8 | % 4
  s4 s8 g8:7 s8 s8 | % 5
  d4:5 s8 s8 s8 s8 | % 6
  s4 s8 s8 s8 s8 | % 7
  c8:5 s8 s8 s8 s8 s8 | % 8
  d8:5 s8 s8 s4. | % 9
  d4:m5 s8 s8 s8 s8 | \barNumberCheck #10
  s8 s8 s8 s8 s8 s8 | % 11
  c4:5 s8 s8 s8 s8 | % 12
  s4 s8 s8 s8 s8 | % 13
  d8:m5 s8 s8 s8 s8 s8 | % 14
  s8 s8 s8 s8 s8 s8 | % 15
  c8:5 s8 s8 s8 s8 s8 | % 16
  d8:5 s8 s8 s4. | % 17
  s2. s2. \bar "|."
}


% The score definition
\score {
  <<
    \context ChordNames = "PartPOneVoiceOneChords" \PartPOneVoiceOneChords
    \new Staff <<

      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

