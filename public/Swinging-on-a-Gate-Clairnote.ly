\version "2.19.49"

% clairnote-type = sn
\include "clairnote.ly"

#(set-default-paper-size "letter")
\pointAndClickOff

% set staff size, 20 is default, 22.45 and 25.2 work well with fonts
% #(set-global-staff-size 22.45  )

\header {
  title = "Swinging on a Gate"
  subtitle = \markup \tiny {Traditional Fiddle Tune, in Clairnote Music Notation }
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
}


melody = \relative c' {
  \clef treble
  \key g \major
  \time 4/4

\partial 4
e'8 fis
\repeat volta 2 {
g e d b g4 a8 b
c (a) b g a g (e g)

d g b d g4 g
fis8 g a g fis d e fis
(g e) d b g4 a8 b

c (a) b g a g (e g)
c a b g a g (e g)
d g g fis (g4) e'8 fis
}

\repeat volta 2 {
g fis g a (b a) g fis
(g fis) e d e d (c b)
e,4 a8 (b c b) a g
e g a b c d e fis

(g fis) g a (b a) g fis
(g fis) e d e d (c b)
c a b g a g (e g)
}
\alternative {
  { d g g fis (g4) e'8 fis }
  { d, g g fis g4 r }
}
}


\score {
  \new Staff \with {
    % \staffSize #1.8
  }
  { \melody }

  \layout {
    indent = 0\in
  }
}

\score {
  \unfoldRepeats  { \melody }
  \midi { \tempo 4 = 100 }
}
