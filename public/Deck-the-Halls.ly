\version "2.19.49"
% automatically converted by musicxml2ly

% clairnote-type = sn
\include "clairnote.ly"

\paper {
  paper-width = 21.01 \cm
  paper-height = 29.69 \cm
  top-margin = 1.0 \cm
  bottom-margin = 2.0 \cm
  left-margin = 1.0 \cm
  right-margin = 1.0 \cm
  system-system-spacing.basic-distance = #20
}

\header {
  title = "Deck the Halls"
  subtitle = " "
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
  encodingsoftware = "MuseScore 2.0.2"
  encodingdate = "2015-11-27"
}

PartPOneVoiceOne =  \relative a' {
  \repeat volta 2 {
    \clef "treble" \key d \major \numericTimeSignature\time 4/4 a4.
    g8 fis4 e4 | % 2
    d4 e4 fis4 d4 | % 3
    e8 fis8 g8 e8 fis4. e8 | % 4
    d4 cis4 d4 r4 | % 5
    a'4. g8 fis4 e4 | % 6
    d4 e4 fis4 d4 | % 7
    e8 fis8 g8 e8 fis4. e8 | % 8
    d4 cis4 d4 r4 | % 9
    e4. fis8 g4 e4 | \barNumberCheck #10
    fis4. g8 a4 e4 | % 11
    fis8 g8 a4 b8 cis8 d4 | % 12
    cis4 b4 a4 r4 | % 13
    a4. g8 fis4 e4 | % 14
    d4 e4 fis4 d4 | % 15
    b'8 b8 b8 b8 a4. g8 | % 16
    fis4 e4 d2
  }
}

PartPOneVoiceOneLyricsOne =  \lyricmode {
  Deck the halls with bows of hol -- ly
  fa la la la la fa la la la
  "'tis" the seas -- on to be jol -- ly
  fa la la la la fa la la la
  fill the mead -- cup drain the bar -- rel
  fa la la fa la la la la la
  troll the an -- cient Christ -- mas ca -- rol
  fa la la la la fa la la la
}
PartPOneVoiceOneLyricsTwo =  \lyricmode {
  see the flow -- ing bowl be -- fore us
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  strike the harp and join the cho -- rus
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  fol -- low me in mer -- ry mea -- sure
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  while I sing of beaut -- "y's" trea -- sure
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
}
PartPOneVoiceOneLyricsThree =  \lyricmode {
  fast a -- way the old year pas -- ses
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  hail the new ye lads and las -- ses
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  laugh -- ing -- quaf -- fing all to -- ge -- ther
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
  heed -- less of the wind and weat -- her
  \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4 \skip4
}
PartPTwoVoiceOne =  \relative fis {
  \repeat volta 2 {
    \clef "bass" \key d \major \numericTimeSignature\time 4/4 fis4.
    e8 d4 a4 | % 2
    fis4 a4 d4 d4 | % 3
    cis8 d8 e8 cis8 d4. cis8 | % 4
    a4 a4 a4 r4 | % 5
    fis'4. e8 d4 a4 | % 6
    fis4 a4 d4 d4 | % 7
    cis8 d8 e8 cis8 d4. cis8 | % 8
    a4 a4 a4 r4 | % 9
    cis4. cis8 cis4 cis4 | \barNumberCheck #10
    d4. d8 d4 cis4 | % 11
    d8 d8 d4 d8 d8 d4 | % 12
    e4 gis4 a4 r4 | % 13
    fis4. e8 d4 a4 | % 14
    fis4 a4 d4 d4 | % 15
    g8 g8 g8 g8 fis4. e8 | % 16
    d4 cis4 d2
  }
}


% The score definition
\score {
  <<
    \new Staff <<
      \set Staff.instrumentName = "Alto"
      \set Staff.shortInstrumentName = "A."
      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsOne
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsTwo
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsThree
      >>
    >>
    \new Staff <<
      \set Staff.instrumentName = "Baritone"
      \set Staff.shortInstrumentName = "Bar."
      \context Staff <<
        \context Voice = "PartPTwoVoiceOne" { \PartPTwoVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

