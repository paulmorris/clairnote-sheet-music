\version "2.19.49"
% automatically converted by musicxml2ly

% clairnote-type = sn
\include "clairnote.ly"

#(set-global-staff-size 20)

\paper {
  paper-width = 21.01 \cm
  paper-height = 29.69 \cm
  top-margin = 1.0 \cm
  bottom-margin = 2.0 \cm
  left-margin = 1.0 \cm
  right-margin = 1.0 \cm
  system-system-spacing.basic-distance = #24
}

\header {
  title = "Silent Night"
  subtitle = " "
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
  encodingsoftware = "MuseScore 2.0.2"
  encodingdate = "2015-11-27"
}

\layout {
  \context {
    \Score
    skipBars = ##t
  }
}

PartPOneVoiceOne =  \relative e' {
  \clef "treble" \key a \major \time 3/4 R2. | % 2
  e4. fis8 e4 | % 3
  cis2 r4 | % 4
  e4. fis8 e4 | % 5
  cis2 r4 | % 6
  b'2 b4 | % 7
  gis2 r4 | % 8
  a2 a4 | % 9
  e2 r4 | \barNumberCheck #10
  fis2 fis4 | % 11
  a4. gis8 fis4 | % 12
  e4. fis8 e4 | % 13
  cis2. | % 14
  fis2 fis4 | % 15
  a4. gis8 fis4 | % 16
  e4. fis8 e4 | % 17
  cis2. | % 18
  b'4. b8 b4 | % 19
  d4. b8 gis4 | \barNumberCheck #20
  a2. | % 21
  cis2. | % 22
  a4. e8 cis4 | % 23
  e4. d8 b4 | % 24
  a2. ~ | % 25
  a4 r2 |
  \bar "|."
}

PartPOneVoiceOneLyricsOne =  \lyricmode {
  Sti -- \skip4 le nacht hei --
  li -- ge nacht al -- les slaapt slui -- mert zacht een -- zaam waakt
  het hoog -- hei -- li -- ge paar lief -- lijk kind -- je met goud in
  het haar slui -- mert in he -- mel -- se -- rust -- \skip4 slui -- mert
  in he -- mel -- se rust
}

PartPOneVoiceOneLyricsTwo =  \lyricmode {
  Si -- \skip4 lent night ho --
  \skip4 ly night all is calm all is bright round yon vir -- \skip4
  gin moth -- er and child ho -- ly in -- fant so ten -- der and mild
  sleep -- \skip4 in heav -- en -- ly peace -- \skip4 sleep -- \skip4
  in heav en -- ly peace
}

PartPTwoVoiceOne =  \relative cis {
  \clef "bass" \key a \major \time 3/4 R2. | % 2
  cis4. d8 cis4 | % 3
  a2 r4 | % 4
  cis4. d8 cis4 | % 5
  a2 r4 | % 6
  gis'2 gis4 | % 7
  e2 r4 | % 8
  cis2 cis4 | % 9
  a2 r4 | \barNumberCheck #10
  d2 d4 | % 11
  fis4. e8 d4 | % 12
  cis4. d8 cis4 | % 13
  a2. | % 14
  d2 d4 | % 15
  fis4. e8 d4 | % 16
  cis4. d8 cis4 | % 17
  a2. | % 18
  gis'4. gis8 gis4 | % 19
  gis4. e8 d4 | \barNumberCheck #20
  cis2. | % 21
  e2. | % 22
  cis'4. a8 e4 | % 23
  gis4. fis8 d4 | % 24
  cis2. ~ | % 25
  cis4 r2 |
  \bar "|."
}


% The score definition
\score {
  <<
    \new Staff <<
      \set Staff.instrumentName = "Alto"
      \set Staff.shortInstrumentName = "A."
      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsOne
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsTwo
      >>
    >>
    \new Staff <<
      \set Staff.instrumentName = "Tenor"
      \set Staff.shortInstrumentName = "T."
      \context Staff <<
        \context Voice = "PartPTwoVoiceOne" { \PartPTwoVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

