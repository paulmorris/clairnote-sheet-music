\version "2.19.49"
% automatically converted by musicxml2ly

% clairnote-type = sn
\include "clairnote.ly"

\paper {
  paper-width = 21.01 \cm
  paper-height = 29.69 \cm
  top-margin = 1.0 \cm
  bottom-margin = 2.0 \cm
  left-margin = 1.0 \cm
  right-margin = 1.0 \cm
  system-system-spacing.basic-distance = #20
}

\header {
  title = "Joy to the World"
  subtitle = " "
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
  encodingsoftware = "MuseScore 2.0.2"
  encodingdate = "2015-11-27"
}

PartPOneVoiceOne =  \relative c'' {
  \clef "treble" \key c \major \time 2/4 | % 1
  \tempo 4=80 c4 b8. a16 | % 2
  g4. f8 | % 3
  e4 d4 | % 4
  c4. g'8 | % 5
  a4. a8 | % 6
  b4. b8 | % 7
  c2 ~ | % 8
  c4 r8 c8 | % 9
  c8 ( b8 ) a8 ( g8 ) | \barNumberCheck #10
  g8. ( f16 ) e8 c'8 | % 11
  c8 ( b8 ) a8 ( g8 ) | % 12
  g8. ( f16 ) e8 e8 | % 13
  e16 e16 e8 e8 e16 ( f16 ) | % 14
  g4. f16 ( e16 ) | % 15
  d16 d16 d8 d8 d16 ( e16 ) | % 16
  f4. e16 ( d16 ) | % 17
  c8 ( c'4 ) a8 | % 18
  g8. ( f16 ) e8 f8 | % 19
  e4 d4 | \barNumberCheck #20
  c2 \bar "|."
}

PartPOneVoiceOneLyricsOne =  \lyricmode {
  Joy to the world the Lord has
  "come." Let earth re -- ceive her King let ev ry -- heart -- \skip4
  pre -- pare -- him -- room -- \skip4 and heav -- en -- and na -- tu
  -- sing and -- hea -- ven and na -- ture sing and -- hea -- and heav
  -- en and na -- ture sing
}

PartPOneVoiceOneLyricsTwo =  \lyricmode {
  Joy to the world the Sa --
  viour reigns Let men their songs em -- ploy while fields -- \skip4
  floods -- \skip4 rocks hills -- \skip4 plains \skip4 re -- peat --
  \skip4 their soun -- ding joy re peat -- \skip4 their soun -- ding
  joy re -- peat -- re -- peat -- \skip4 their soun -- ding joy
}

PartPOneVoiceOneLyricsThree =  \lyricmode {
  He rules the world with
  truth and grace and makes the nat -- ions prove the glo of right --
  ness and won -- of love -- \skip4 and won -- \skip4 ders of his --
  love and won -- \skip4 ders of his love and won -- and won -- \skip4
  ders of his love
}

PartPTwoVoiceOne =  \relative e' {
  \clef "treble" \key c \major \time 2/4
  % \override Stem.direction = #DOWN
  e4 g8. f16 | % 2
  e4. d8 | % 3
  c4 b4 | % 4
  c4. g'8 | % 5
  f4. f8 | % 6
  f4. f8 | % 7
  e2 ~ | % 8
  e4 r8 e8 | % 9
  e8 ( g8 ) f8 ( e8 ) | \barNumberCheck #10
  e8. ( d16 ) c8 e8 | % 11
  e8 ( g8 ) f8 ( e8 ) | % 12
  e8. ( d16 ) c8 c8 | % 13
  c8 c8 c8 c16 ( d16 ) | % 14
  e4. d16 ( c16 ) | % 15
  b8 b8 b8 b16 ( c16 ) | % 16
  d4. c16 ( b16 ) | % 17
  c8 ( e4 ) f8 | % 18
  e8. ( d16 ) c8 d8 | % 19
  c4 b4 | \barNumberCheck #20
  c2 \bar "|."
}


% The score definition
\score {
  <<
    \new Staff <<
      \set Staff.instrumentName = "Alto"
      \set Staff.shortInstrumentName = "A."
      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsOne
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsTwo
        \new Lyrics \lyricsto "PartPOneVoiceOne" \PartPOneVoiceOneLyricsThree
      >>
    >>
    \new Staff <<
      \set Staff.instrumentName = "Soprano"
      \set Staff.shortInstrumentName = "S."
      \context Staff <<
        \context Voice = "PartPTwoVoiceOne" { \PartPTwoVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

