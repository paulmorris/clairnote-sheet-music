
\version "2.19.49"
% automatically converted by musicxml2ly from C:/Users/Willem/Documents/muziek/musescore bestanden/Last_Lesson.xml
% clairnote-type = sn
\include "clairnote.ly"
\header {
  encodingsoftware = "MuseScore 2.0.2"
  source = "https://musescore.com/score/1209786"
  encodingdate = "2015-09-16"
  title = "Last Lesson"
  subtitle = "Irish Jig"
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
}

\layout {
  \context {
    \Score
    skipBars = ##t
  }
}
PartPOneVoiceOne =  \relative d' {
  \repeat volta 2 {
    \clef "treble" \key d \major \time 6/8 | % 1
    d8 fis8 a8 a8 b8 a8 | % 2
    fis'8 g8 fis8 fis8 e8 d8 | % 3
    d,8 fis8 a8 a8 b8 a8 | % 4
    d8 cis8 b8 a8 fis8 e8 | % 5
    d8 fis8 a8 a8 b8 a8 | % 6
    fis'8 g8 fis8 fis8 e8 d8
  }
  \alternative {
    {
      | % 7
      b'8 g8 e8 fis8 a8 g8 | % 8
      fis8 e8 d8 cis8 b8 a8
    }
    {
      | % 9
      g'8 b8 g8 fis8 a8 fis8
    }
  } | \barNumberCheck #10
  e8 g8 fis8 e8 d8 cis8 | % 11
  d8 fis8 fis8 fis8 g8 fis8 | % 12
  a8 b8 a8 g8 fis8 e8 | % 13
  d8 fis8 fis8 g8 fis8 e8 | % 14
  d8 cis8 b8 a8 fis8 e8 | % 15
  d'8 fis8 fis8 fis8 g8 fis8 | % 16
  fis8 g8 fis8 fis8 g8 fis8 | % 17
  b8 g8 e8 fis8 a8 g8 | % 18
  fis8 e8 d8 a8 b8 cis8 | % 19
  d8 fis8 fis8 fis8 g8 fis8 | \barNumberCheck #20
  a8 b8 a8 g8 fis8 e8 | % 21
  d8 fis8 fis8 fis8 g8 fis8 | % 22
  a8 g8 fis8 e8 cis8 a8 | % 23
  d8 fis8 fis8 fis8 g8 fis8 | % 24
  fis8 g8 fis8 fis8 g8 fis8 | % 25
  a8 e8 g8 e8 fis8 e8 | % 26
  d8 cis8 b8 a4. \bar "|."

}

PartPOneVoiceOneChords =  \chordmode {
  \repeat volta 2 {
    | % 1
    d8:5 s8 s8 s8 s8 s8 | % 2
    s8 s8 s8 s8 s8 s8 | % 3
    s8 s8 s8 s8 s8 s8 | % 4
    g8:5 s8 s8 a8:5 s8 s8 | % 5
    d8:5 s8 s8 s8 s8 s8 | % 6
    s8 s8 s8 s8 s8 s8
  }
  \alternative {
    {
      | % 7
      g8:5 s8 s8 d8:5 s8 s8 | % 8
      e8:m7 s8 s8 a8:5 s8 s8
    }
    {
      | % 9
      g8:5 s8 s8 d8:5 s8 s8
    }
  } | \barNumberCheck #10
  e8:m7 s8 s8 a8:5 s8 s8 | % 11
  d8:5 s8 s8 s8 s8 s8 | % 12
  g8:5 s8 s8 a8:5 s8 s8 | % 13
  d8:5 s8 s8 s8 s8 s8 | % 14
  g8:5 s8 s8 a8:5 s8 s8 | % 15
  d8:5 s8 s8 s8 s8 s8 | % 16
  s8 s8 s8 s8 s8 s8 | % 17
  g8:5 s8 s8 d8:5 s8 s8 | % 18
  s8 s8 s8 a8:5 s8 s8 | % 19
  d8:5 s8 s8 s8 s8 s8 | \barNumberCheck #20
  g8:5 s8 s8 a8:5 s8 s8 | % 21
  d8:5 s8 s8 s8 s8 s8 | % 22
  a8:5 s8 s8 s8 s8 s8 | % 23
  d8:5 s8 s8 s8 s8 s8 | % 24
  s8 s8 s8 s8 s8 s8 | % 25
  g8:5 s8 s8 s8 s8 s8 | % 26
  e8:m7 s8 s8 a4. \bar "|."

}


% The score definition
\score {
  <<
    \context ChordNames = "PartPOneVoiceOneChords" \PartPOneVoiceOneChords
    \new Staff <<

      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

