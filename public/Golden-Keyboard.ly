\version "2.19.49"
% automatically converted by musicxml2ly from C:/Users/Willem/Documents/muziek/musescore bestanden/Golden_Keyboard.xml
% clairnote-type = sn
\include "clairnote.ly"
\header {
  encodingsoftware = "MuseScore 2.0.2"
  source = "https://musescore.com/score/1209691"
  encodingdate = "2015-09-16"
  title = "Golden Keyboard"
  subtitle = " "
  tagline = \markup \teeny {
    \with-url \clairnoteTypeUrl { \clairnoteTypeName music notation }
    | Sheet music engraved using \with-url #"http://www.lilypond.org" { LilyPond }
  }
}

\layout {
  \context {
    \Score
    skipBars = ##t
  }
}
PartPOneVoiceOne =  \relative b' {
  \repeat volta 2 {
    \clef "treble" \key g \major \numericTimeSignature\time 4/4 | % 1
    b8 ^\markup{ \bold {Swing} } g8 e8 fis8 g8 fis8 g8 a8 | % 2
    b4 g'8 b,8 fis'8 b,8 e8 d8 | % 3
    b8 g8 e8 fis8 g8 b8 a8 g8 | % 4
    \once \override TupletBracket.stencil = ##f
    \times 2/3  {
      fis8 e8 d8
    }
    a'8 d,8 b'8 d,8 a'8 fis8 | % 5
    e8 d8 e8 fis8 g8 fis8 g8 a8 | % 6
    b4 g'8 b,8 fis'8 b,8 e8 cis8 | % 7
    d4 cis8 e8 d8 b8 a8 fis8 | % 8
    d8 fis8 a8 fis8 g8 e8 e4
  }
  \repeat volta 2 {
    | % 9
    b'8 e8 \once \override TupletBracket.stencil = ##f
    \times 2/3  {
      e8 fis8 e8
    }
    b8 e8 \once \override TupletBracket.stencil = ##f
    \times 2/3  {
      e8 fis8 e8
    }
    | \barNumberCheck #10
    b8 e8 e8 fis8 e8 d8 b8 e8 | % 11
    d8 a8 \once \override TupletBracket.stencil = ##f
    \times 2/3  {
      a8 b8 a8
    }
    e'8 a,8 cis8 e8 | % 12
    d4 cis8 e8 d8 b8 a8 fis8 | % 13
    e8 d8 e8 fis8 g8 fis8 g8 a8 | % 14
    b4 g'8 b,8 fis'8 b,8 e8 cis8 | % 15
    d4 cis8 e8 d8 b8 a8 fis8 | % 16
    d8 fis8 a8 fis8 g8 e8 e4
  }
  | % 17

}

PartPOneVoiceOneChords =  \chordmode {
  \repeat volta 2 {
    | % 1
    e8:m5 s8 s8 s8 s8 s8 s8 s8 | % 2
    s4 s8 s8 d8:5 s8 s8 s8 | % 3
    e8:m5 s8 s8 s8 s8 s8 s8 s8 | % 4
    d8*2/3:5 s1*1/12 s1*1/12 s8 s8 s8 s8 s8 s8 | % 5
    e8:m5 s8 s8 s8 s8 s8 s8 s8 | % 6
    s4 s8 s8 d8:5 s8 s8 s8 | % 7
    g4:5 s8 s8 d8:5 s8 s8 s8 | % 8
    s8 s8 s8 s8 e8:m5 s8 s4
  }
  \repeat volta 2 {
    | % 9
    e8:m5 s8 s1*1/12 s1*1/12 s1*1/12 s8 s8 s1*1/12 s1*1/12 s1*1/12 |
    \barNumberCheck #10
    s8 s8 s8 s8 s8 s8 s8 s8 | % 11
    d8:5 s8 s1*1/12 s1*1/12 s1*1/12 a8:5 s8 s8 s8 | % 12
    d4:5 s8 s8 s8 s8 s8 s8 | % 13
    e8:m5 s8 s8 s8 s8 s8 s8 s8 | % 14
    s4 s8 s8 d8:5 s8 s8 s8 | % 15
    g4:5 s8 s8 d8:5 s8 s8 s8 | % 16
    e8:m5 s8 s8 s8 s8 s8 s4
  }
  | % 17
  s1 s1 s1 s1 \bar "|."
}


% The score definition
\score {
  <<
    \context ChordNames = "PartPOneVoiceOneChords" \PartPOneVoiceOneChords
    \new Staff <<

      \context Staff <<
        \context Voice = "PartPOneVoiceOne" { \PartPOneVoiceOne }
      >>
    >>

  >>
  \layout {}
  \midi {}
}

